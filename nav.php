<?php
  $has_log = false;
  $username = "";
  $type = "";
  if (isset($_SESSION['id_user'])){
    $has_log = true;

    $mysql = new MysqlConnection();
    $username = $mysql->getUsername($_SESSION['id_user']);
    $type = $mysql->getTypeUser($_SESSION['id_user']);
  }
?>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="?t=home">Saw</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item"><a href="?t=note" class="nav-link">Add Note</a></li>
      <li class="nav-item"><a href="?t=listNote" class="nav-link">List Notes</a></li>
      <?php if($type === "ROLE_ADMIN"): ?>
      <li class="nav-item"><a href="?t=listUsers" class="nav-link">List Users</a></li>
      <?php endif; ?>
    </ul>
    <div class="form-inline my-2 my-lg-0">
      <?php if(!$has_log): ?>
      <a class="btn btn-success mr-sm-2" href="?t=login">Login</a>
      <a class="btn btn-outline-secondary my-2 my-sm-0" href="?t=register">Register</a>
      <?php else: ?>
      <a class="btn btn-primary mr-sm-2" href="?t=profile"><?php echo $username ?></a>
      <a class="btn btn-outline-danger my-2 mr-sm-0" href="?t=logout">Logout</a>
      <?php endif; ?>
    </div>
  </div>
</nav>