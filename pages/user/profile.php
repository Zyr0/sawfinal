<?php
  $mysql = new MysqlConnection();
  $user = $mysql->getUserInfo($_SESSION['id_user']);
?>

<div class="jumbotron mt-4" style="width: 100%">
  <h1 class="display-4"><?php echo $user['username'] ?></h1>
  <div class="row lead">
    <div class="col-md-2">
      <img class="img-fluid" src="<?php echo $user['img'] ?>" alt="user image">
    </div>
    <div class="col-md-8">
      <b>Name: </b> <?php echo $user['name'] ?> <br>
      <b>Email: </b> <?php echo $user['email'] ?> <br>
      <b>Number of Notes: </b> <?php echo $user['number_notes'] ?> <br>
    </div>
  </div>
  <hr class="my-4">
  <p class="lead">
    <a class="btn btn-primary btn-lg" href="?t=edit_profile" role="button">Edit</a>
  </p>
  <p class="lead">
    <form method="GET" action="?t=recovery">
      <div class="form-group">
        <input type="hidden" name="t" value="recovery">
        <input type="hidden" name="email" value="<?php echo $user['email'] ?>">
        <input type="hidden" name="h" value=<?php echo sha1($user['email'] . rand(0, rand(0, 99999))); ?>>
      </div>
      <input type="submit" class="btn btn-primary btn-lg" value="Change Password">
    </form>
  </p>
</div>