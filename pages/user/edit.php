<?php

  $validator = new Validator();
  $mysql = new MysqlConnection();

  $hasMysqlErr = false;
  $result = "no";
  $err = array("Invalid" => "<strong>Invalid Form</strong>");
  $hasErr = false;
  $file_name = "Choose avatar";
  $hasFile = true;
  $fileErr = false;
  $email = false;

  $user = $mysql->getUserInfo($_SESSION['id_user']);
  if ($_SERVER['REQUEST_METHOD'] == "POST"){
    try{
      $err['name'] = $validator->validate_name($_POST['name']);
      $err['username'] = $validator->validate_username($_POST['username']);
      $err['email'] = $validator->validate_email($_POST['email']);
      $name = $validator->sanitize($_POST['name']);
      $username = $validator->sanitize($_POST['username']);
      $email = $validator->sanitize($_POST['email']);

      if (isset($_FILES['avatar'])){
        $file_name = $_FILES['avatar']['name'];
        $file_size = $_FILES['avatar']['size'];
        $file_tmp = $_FILES['avatar']['tmp_name'];
        $file_type = $_FILES['avatar']['type'];
        $file_ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
        $expensions= array("jpeg","jpg","png");

        if ($file_name === ""){
          $hasFile = false;
        }

        if (in_array($file_ext, $expensions) === false && $hasFile){
          $err['imageExt'] = "File type not allowed";
          $hasErr = true;
          error_log("Error  File type not allowed", 0);
        }

        if ($file_size > 500000 && $hasFile) {
          $err['imageSize'] = "Image is too large.";
          $fileErr = true;
          error_log("Error  Image is too large", 0);
        }
      } else {
        $hasFile = false;
      }

      if ($err['name'] != "" && $err['username'] != "" && $err['email'] != ""){
        $hasErr = true;
        error_log("Error in name, username or email", 0);
      }

      if (!$hasErr or !$fileErr){
        $final_path = $user['img'];
        if ($hasFile) {
          if(file_exists($final_path) && $file_name !== "default"){
            unlink($final_path);
          }
          $final_path = "upload/images/" . $username . "." . $file_name;
          move_uploaded_file($file_tmp, $final_path);
        }

        $result = $mysql->updateUser($_SESSION['id_user'], $name, $username, $email, $final_path);
        if ($user['email'] !== $email){
          $val = $email . rand(0, rand(0, 99999));
          $hash = sha1($val);
          $mysql->addHash($email, $hash);
          $res = $mysql->unverifyUser($_SESSION['id_user']);
          if ($res === "OK") {
            $to = '' . $email . '';
            $subject = 'I Saw Notes';
            $message='Please click this link to activate your account: https://www.saw.com/?t=verify&email=' . $email . '&h=' . $hash;

            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
            $headers .= 'From: I Saw Notes <dwdmsawfinalcj@gmail.com>' . "\r\n";
            mail($to, $subject, $message, $headers);
            $email = true;
          }
        }
      }
    }
    catch(Exception $e){
      $hasMysqlErr = true;
      $mysqlErrMessage = $e->getMessage();
      error_log("Error  " . $mysqlErrMessage, 0);
    }
  }
?>
<div class="row">
  <div class="col-12">
    <?php if($hasMysqlErr or $hasErr): ?>
    <div class="alert alert-danger alert-dismissable fade show mt-2">
      <strong>Who!</strong>
      <?php
      if ($hasMysqlErr) {
        echo $mysqlErrMessage;
      } else {
        foreach($err as $e => $e_val){
          if ($e_val != ""){
            echo $e_val . " ";
          }
        }
      }
      ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php endif; ?>
    <?php if($result === "OK" && !$hasMysqlErr): ?>
    <div class="alert alert-success alert-dismissable fade show mt-2" style="width: 100%">
      Updated with success
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php endif; ?>
    <?php if($email): ?>
    <div class="alert alert-success alert-dismissable fade show mt-2" style="width: 100%">
      Please verify you email
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php endif; ?>
  </div>
</div>

<section class="col-12" id="register">
  <h1 class="display-3 text-center">Update</h1>
  <div class="row mt-4">
    <div class="col-12">
      <form role="form" method="post" id="update-form" autocomplete="off" accept-charset='UTF-8' enctype = "multipart/form-data"  novalidate>
        <div class="row">
          <div class="col-md-6 col-xs-12">
            <label for="img">Upload Avatar</label>
            <div class="input-group">
              <div class="custom-file">
                <input type="file" name="avatar" class="custom-file-input" id="img" aria-describedby="img">
                <label class="custom-file-label" for="img"><?php echo $file_name ?></label>
              </div>
            </div>
            <small id="avaHel" class="form-text text-muted"> Please choose a JPEG or PNG file. </small>
          </div>
          <div class="col-md-6 col-xs-12">
            <div class="form-group">
              <label for="name">Name *</label>
              <input type="text" name="name" id="name" class="form-control" placeholder="name" value="<?php echo $user['name'] ?>" required>
            </div>
            <div class="form-group">
              <label for="username">Username *</label>
              <input type="text" name="username" id="username" class="form-control" placeholder="Username" value="<?php echo $user['username'] ?>" required>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="email">Email *</label>
          <input type="email" name="email" id="email" class="form-control" placeholder="somebody@example.com" value="<?php echo $user['email'] ?>" required>
        </div>
        <div class="text-right">
          <input type="submit" id="btn-edit" class="btn btn-primary btn-lg pull-right" value="Edit">
        </div>
      </form>
    </div> <!-- /.col-6 -->
  </div> <!-- /.row -->
</section>