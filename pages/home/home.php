<div id="bg"></div>
<section id="main" class="col-12">
  <section id="card">
    <ul>
      <li>
        <span></span>
        <strong>All private</strong>
      </li>
      <li>
        <span></span>
        <strong>Keep your notes</strong>
      </li>
      <li>
        <span></span>
        <strong>Edit your notes</strong>
      </li>
      <li>
        <span></span>
        <strong>Delete your notes</strong>
      </li>
    </ul>
  </section>
  <section id="primary">
    <h1>Your notes manager</h1>
    <p>Write down so you don't forget.</p>
    <a href="?t=register">Register</a>
  </section>
</section>