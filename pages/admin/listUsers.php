<?php
  $mysql = new MysqlConnection();
  $users = $mysql->getAllUsers();

  $count = 0;
  if($users != null){
    $count = count($users);
  }

  $hasMysqlErr = false;
  $mysqlErrMessage = "";
  $result = "";
  $err = false;

  if ($_SERVER['REQUEST_METHOD'] == "POST"){
    try {
      if ($_SESSION['id_user'] != $_POST['id']){
        $result = $mysql->deleteUser($_POST['id']);
        if ($result === "OK"){
          header("Refresh:0");
        }
      } else {
        $err = true;
      }
    } catch(Exception $e){
      $hasMysqlErr = true;
      $mysqlErrMessage = $e->getMessage();
      error_log("Error  " . $mysqlErrMessage, 0);
    }
  }
?>

<section class="col-12">
  <?php if($hasMysqlErr): ?>
  <div class="alert alert-danger alert-dismissable fade show mt-2">
    <strong>Who!</strong> <?php echo $mysqlErrMessage; ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <?php elseif($err): ?>
  <div class="alert alert-danger alert-dismissable fade show mt-2">
    <strong>Who!</strong> You can't eliminate yourself
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <?php elseif($result === "OK"): ?>
  <div class="alert alert-success alert-dismissable fade show mt-2">
    <strong>Who!</strong> User eliminated with success
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <?php endif; ?>
</section>

<section class="col-12">
  <br>
  <table class="table table-striped table-bordered">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Number Notes</th>
      <th scope="col">Name</th>
      <th scope="col">Username</th>
      <th scope="col">Email</th>
      <th scope="col">Role</th>
      <th scope="col">Verified</th>
      <th scope="col">Options</th>
    </th>
  </thead>
  <tbody>
    <?php $i=0; while($i < $count): ?>
    <tr>
      <th scope="row"><?php echo $users[$i]['id_user'] ?></th>
      <th><?php echo $users[$i]['number_notes'] ?></th>
      <th><?php echo $users[$i]['name'] ?></th>
      <th><?php echo $users[$i]['username'] ?></th>
      <th><?php echo $users[$i]['email'] ?></th>
      <th><?php echo $users[$i]['type'] ?></th>
      <?php if ($users[$i]['verified'] === 1): ?>
        <th>Yes</th>
      <?php else: ?>
        <th>No</th>
      <?php endif; ?>
      <th>
        <form method="post">
          <input type="hidden" name="id" value="<?php echo $users[$i]['id_user'] ?>">
          <input type="submit" class="btn btn-danger" value="Delete User">
        </form>
      </th>
    </tr>
    <?php $i++; endwhile; ?>
  </tbody>
  </table>
</section>