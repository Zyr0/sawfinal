<?php

  $validator = new Validator();
  $mysql = new MysqlConnection();

  $hasMysqlErr = false;
  $result = "no";
  $err = array("Invalid" => "<strong>Invalid Form</strong>");
  $hasErr = false;
  $file_name = "Choose avatar";
  $fileErr = false;
  $hasFile = true;

  if ($_SERVER['REQUEST_METHOD'] == "POST"){
    try{
      $err['name'] = $validator->validate_name($_POST['name']);
      $err['username'] = $validator->validate_username($_POST['username']);
      $err['email'] = $validator->validate_email($_POST['email']);
      $err['password'] = $validator->validate_password($_POST['pass']);
      $name = $validator->sanitize($_POST['name']);
      $username = $validator->sanitize($_POST['username']);
      $email = $validator->sanitize($_POST['email']);
      $password = $validator->sanitize($_POST['pass']);
      $rePassword = $validator->sanitize($_POST['rePass']);

      if (isset($_FILES['avatar'])){
        $file_name = $_FILES['avatar']['name'];
        $file_size = $_FILES['avatar']['size'];
        $file_tmp = $_FILES['avatar']['tmp_name'];
        $file_type = $_FILES['avatar']['type'];
        $file_ext = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
        $expensions= array("jpeg","jpg","png");

        if ($file_name === ""){
          $hasFile = false;
        }

        if (in_array($file_ext, $expensions) === false && $hasFile){
          $err['imageExt'] = "File type not allowed";
          $hasErr = true;
          error_log("Error File not allowed", 0);
        }

        if ($file_size > 500000 && $hasFile) {
          $err['imageSize'] = "Image is too large.";
          $fileErr = true;
          error_log("Error  Image to large", 0);
        }
      }

      $err['repass'] = "";
      if ($password !== $rePassword){
        $err['repass'] = "Passwords do not match";
        error_log("Error  Passwords do not match", 0);
      }

      if ($err['username'] != "" or $err['email'] != "" or $err['password'] != "" or $err['repass'] != ""){
        $hasErr = true;
        error_log("Error  " . $err, 0);
      }

      if (!$hasErr && !$fileErr){

        $final_path = "upload/images/default.jpg";
        if ($hasFile) {
          $final_path = "upload/images/" . $username . "." . $file_name;
          move_uploaded_file($file_tmp, $final_path);
        }

        $val = $email . rand(0, rand(0, 99999));
        $hash = sha1($val);
        $result = $mysql->createUser($name, $username, $email, $password, $final_path, $hash);

        if ($result === "OK"){
          $to = '' . $email . '';
          $subject = 'I Saw Notes';
          $message='Please click this link to activate your account: https://www.saw.com/?t=verify&email=' . $email . '&h=' . $hash;

          $headers  = 'MIME-Version: 1.0' . "\r\n";
          $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
          $headers .= 'From: I Saw Notes <dwdmsawfinalcj@gmail.com>' . "\r\n";
          mail($to, $subject, $message, $headers);
        }
      }
    }
    catch(Exception $e){
      $hasMysqlErr = true;
      $mysqlErrMessage = $e->getMessage();
      error_log("Error  " . $mysqlErrMessage, 0);
    }
  }
?>

<!-- errors -->
<div class="row">
  <div class="col-12">
    <?php if($hasMysqlErr or $hasErr): ?>
    <div class="alert alert-danger alert-dismissable fade show mt-2">
      <strong>Who!</strong>
      <?php
      if ($hasMysqlErr) {
        echo $mysqlErrMessage;
      } else {
        foreach($err as $e => $e_val){
          if ($e_val != ""){
            echo $e_val . " ";
          }
        }
      }
      ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php endif; ?>
    <?php if($result === "OK" && !$hasMysqlErr): ?>
    <div class="alert alert-success alert-dismissable fade show mt-2">
      <strong>Who!</strong> Welcome to this website please verify your email
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php endif; ?>
  </div>
</div>
<!-- end errors -->

<!-- content -->
<section class="col-md-8 col-xs-12" id="register">
  <h1 class="display-3 text-center">Register</h1>
  <form role="form" method="post" id="register-form" autocomplete="off" accept-charset='UTF-8' enctype = "multipart/form-data" novalidate>
    <div class="row">
      <div class="col-md-6 col-xs-12">
        <label for="img">Upload Avatar</label>
        <div class="input-group">
          <div class="custom-file">
            <input type="file" name="avatar" class="custom-file-input" id="img" aria-describedby="img">
            <label class="custom-file-label" for="img"><?php echo $file_name ?></label>
          </div>
        </div>
        <small id="avaHel" class="form-text text-muted"> Please choose a JPEG or PNG file. </small>
      </div> <!-- col-md-6 col-xs-12 -->
      <div class="col-md-6 col-xs-12">
        <div class="form-group">
          <label for="name">Name *</label>
          <input type="text" name="name" id="name" class="form-control" placeholder="Name" required>
        </div>
        <div class="form-group">
          <label for="username">Username *</label>
          <input type="text" name="username" id="username" class="form-control" placeholder="Username" required>
        </div>
      </div>
    </div> <!-- col-md-6 col-xs-12 -->
    <div class="form-group">
      <label for="email">Email *</label>
      <input type="email" name="email" id="email" class="form-control" placeholder="somebody@example.com" required>
    </div>
    <div class="row">
      <div class="col-md-6 col-xs-12">
        <div class="form-group">
          <label for="pass">Password *</label>
          <input type="password" name="pass" id="pass" class="form-control" placeholder="Password" aria-describedby="passHelp" require>
          <small id="passHelp" class="form-text text-muted">Password must be 8-20 long.</small>
        </div>
      </div>
      <div class="col-md-6 col-xs-12">
        <div class="form-group">
          <label for="rePass">Re password *</label>
          <input type="password" name="rePass" id="rePass" class="form-control" placeholder="Re password" aria-describedby="rePassHelp" require>
          <small id="rePassHelp" class="form-text text-muted"> Re enter your password. </small>
        </div>
      </div>
    </div>
    <div class="text-right">
      <input type="submit" id="btn-login" class="btn btn-primary btn-lg pull-right" value="Register">
    </div>
  </form>
</section>
<!-- end content -->
