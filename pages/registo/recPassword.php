<?php

  $mysql = new MysqlConnection();
  $validator = new Validator();
  $mysqlErrMessage = "";
  $hasMysqlErr = false;
  $hasErr = false;

  try {
    $updatable = $mysql->checkExpiration($_GET['email'], $_GET['h']);
  } catch(Exception $e){
    $hasMysqlErr = true;
    $mysqlErrMessage = $e->getMessage();
    error_log("Error  " . $mysqlErrMessage, 0);
  }

  if($_SERVER["REQUEST_METHOD"] == "POST") {
    $email = $_GET['email'];

    $result = "no";
    $err = array("Invalid" => "<strong>Invalid Form</strong>");
    $hasErr = false;

    $err['pass'] = $validator->validate_password($_POST['pass']);
    $pass = $validator->sanitize($_POST['pass']);
    $repass = $validator->sanitize($_POST['repass']);

    if ($err['pass'] != "" ){
      $hasErr = true;
    }

    if($repass !== $pass ){
      $err['notMatch'] = "Password's don't match";
    }

    try {
      if (!$hasErr && $updatable){
        $result = $mysql->updatePassword($email, $pass);
        if ($result === "OK"){
          header("Location: ?t=recovery&success");
        }
      }
    } catch(Exception $e){
      $hasMysqlErr = true;
      $mysqlErrMessage = $e->getMessage();
      error_log("Error  " . $mysqlErrMessage, 0);
    }
}
?>
<section class="col-12">
  <?php if($hasMysqlErr or $hasErr): ?>
    <div class="alert alert-danger alert-dismissable fade show mt-2">
      <strong>Who!</strong>
      <?php
      if ($hasMysqlErr) {
        echo $mysqlErrMessage;
      } else {
        foreach($err as $e => $e_val){
          if ($e_val != ""){
            echo $e_val . " ";
          }
        }
      }
      ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php endif; ?>
</section>

<section class="col-6 col-xs-12">
  <h1 class="display-3 text-center">Change Password</h1>
  <form class="mt-4 needs-validation" role="form" method="post" id="newPass-form" autocomplete="off" accept-charset='UTF-8' novalidate>
    <div class="form-group">
      <input type="password" name="pass" id="pass" class="form-control" placeholder="New Password" require>
    </div>
    <div class="form-group">
      <input type="password" name="repass" id="repass" class="form-control" placeholder="Repeat Password" require>
    </div>
    <div class="form-group">
      <input type="hidden" name="h" class="form-control" value="<?php echo $_GET['h'] ?>" require>
    </div>
    <button type="submit" class="btn btn-primary btn-lg float-right" name="submit" value="Submit" id="submit_form">Comfirm</button>
  </form>
</section>