<?php

  if(isset($_SESSION['id_user'])){
    session_destroy();
  }

  $mysql = new MysqlConnection();

  $hasMysqlErr = false;
  $mysqlErrMessage = "";
  $hasErr = false;
  $send = false;
  $changed = false;

  if (isset($_GET['email']) && isset($_GET['h'])) {
    $email = $_GET['email'];
    $hash = $_GET['h'];

    try {
      $try = $mysql->contain($email);
      if ($try === $email){
        $result = $mysql->addHash($email, $hash);
        if ($result === "OK"){
          emailPass($email, $hash);
          $send = true;
        }
      } else {
        $hasErr = true;
      }
    } catch(Exception $e) {
      $hasMysqlErr = true;
      $mysqlErrMessage = $e->getMessage();
      error_log("Error  " . $mysqlErrMessage, 0);
    }
  } else if (isset($_GET['success'])) {
    $changed = true;
  }

  function emailPass($email, $hash) {
    $to = $email;
    $subject = 'I Saw Notes but no Password';
    $message = 'Please click this link to Reset your Password: https://www.saw.com/?t=recPassword&email=' . $email . '&h=' . $hash;
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
    $headers .= 'From: I Saw Notes <dwdmsawfinalcj@gmail.com>' . "\r\n";
    mail($to, $subject, $message, $headers);
  }
?>

<section class="col-12">
  <?php if($hasMysqlErr): ?>
  <div class="alert alert-danger alert-dismissable fade show mt-2">
    <strong>Who!</strong> <?php echo $mysqlErrMessage; ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <?php elseif($hasErr): ?>
  <div class="alert alert-success alert-dismissable fade show mt-2">
    <strong>Who!</strong> Somthing whent wrong
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <?php elseif(!$hasErr && $send): ?>
  <div class="alert alert-success alert-dismissable fade show mt-2">
  <strong>Who!</strong> Check your email too reset your password
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <?php elseif($changed): ?>
  <div class="alert alert-success alert-dismissable fade show mt-2">
  <strong>Who!</strong> Your password has successful changed
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <?php endif; ?>
</section>