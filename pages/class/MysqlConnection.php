<?php
class MysqlConnection
{
  private $servername;
  private $username;
  private $password;
  private $db;
  private $conn;

  public function __construct()
  {
    $this->servername = 'www.saw.com';
    $this->username = 'root';
    $this->password = 'password';
    $this->db = 'saw';
    $this->conn = new mysqli($this->servername, $this->username, $this->password, $this->db);
  }

  public function createTables()
  {
    $sql = "CREATE TABLE type_user(
      id_type INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
      var_type VARCHAR(10)
    );";

    if ($this->conn->query($sql) === true) {
      echo "OK(type user)";
    } else {
      echo "Error" . $this->conn->error;
      error_log("Error creting type user", 0);
    }

    $sql = "INSERT INTO type_user(var_type) values('ROLE_USER'),('ROLE_ADMIN');";

    if ($this->conn->query($sql) === true) {
      echo "OK(pp)";
    } else {
      echo "Error" . $this->conn->error;
      error_log("Error adding type users table", 0);
    }

    $sql = "CREATE TABLE user(
      id_user INT NOT NULL PRIMARY KEY auto_increment,
      name VARCHAR(250) NOT NULL,
      username VARCHAR(250) NOT NULL,
      pass VARCHAR(250) NOT NULL,
      email VARCHAR(250) NOT NULL,
      verified BOOLEAN NOT NULL DEFAULT FALSE,
      img VARCHAR(250) NOT NULL
    );";

    if ($this->conn->query($sql) === true) {
      echo "OK(user)";
    } else {
      echo "Error" . $this->conn->error;
      error_log("Error creating user table", 0);
    }

    $sql = "CREATE TABLE conn_type_user(
      id_conn_type_user INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
      id_user INT NOT NULL,
      id_type INT NOT NULL,
      FOREIGN KEY (id_user) REFERENCES user(id_user),
      FOREIGN KEY (id_type) REFERENCES type_user(id_type)
    );";

    if ($this->conn->query($sql) === true) {
      echo "OK(conn_type_user)";
    } else {
      echo "Error" . $this->conn->error;
      error_log("Error creating conn_type_user table", 0);
    }

    $sql = "CREATE TABLE validator(
      id INT NOT NULL PRIMARY KEY auto_increment,
      hash VARCHAR(250) NOT NULL,
      data_ct TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      data_end TIMESTAMP,
      email VARCHAR(250) NOT NULL
    );";

    if ($this->conn->query($sql) === true) {
      echo "OK(validator)";
    } else {
      echo "Error" . $this->conn->error;
      error_log("Error creating validator table", 0);
    }

    $sql = "CREATE TABLE note(
      id INT NOT NULL PRIMARY KEY auto_increment,
      name VARCHAR(100) NOT NULL,
      text MEDIUMTEXT NOT NULL,
      published_on TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      last_edit TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      id_user INT NOT NULL ,
      FOREIGN KEY (id_user) REFERENCES user(id_user)
    );";

    if ($this->conn->query($sql) === true) {
      echo "OK(note)";
    } else {
      echo "Error" . $this->conn->error;
      error_log("Error creating note table", 0);
    }
  }

  private function insertTypeUsers($type)
  {
    $sql = "SELECT var_type from type_user where var_type = ?";

    if ($stmt = $this->conn->prepare($sql)){
      $stmt->bind_param("s", $type);
      $stmt->execute();
      $result = $stmt->get_result();
      if($result->num_rows > 0){
        throw new Exception('User type already exists');
      }
      $stmt->close();
    }

    $sql = "INSERT INTO type_user(var_type) values(?)";

    if ($stmt = $this->conn->prepare($sql)){
      $stmt->bind_param("s", $type);
      $stmt->execute();
      if ($stmt->affected_rows === 0){
        throw new Exception('Error in execution');
      }
      else {
        $result = "Ok";
      }
      $stmt->close();
      return $result;
    }
  }

  public function createUser($name, $username, $email, $password, $img, $hash){
    $pass = password_hash($password, PASSWORD_BCRYPT);

    $sql_search = "SELECT * FROM user WHERE email = ? OR username = ?";
    if ($stmt = $this->conn->prepare($sql_search)){
      $stmt->bind_param("ss", $email, $username);
      $stmt->execute();
      $result = $stmt->get_result();
      if($result->num_rows > 0){
        throw new Exception('Username or email already exist');
      }

      $stmt->close();
    }

    $sql_insert = "INSERT INTO user (name, username, email, pass, img) values(?,?,?,?,?)";
    if ($stmt = $this->conn->prepare($sql_insert)){
      $stmt->bind_param("sssss", $name, $username, $email, $pass, $img);
      $stmt->execute();

      if ($stmt->affected_rows > 0) {
        $this->addUserType($email);
      } else {
        throw new Exception('Error in execution');
      }
      $stmt->close();
    }

    return $this->addHash($email, $hash);
  }

  public function isVerified($id){
    $sql_search = "SELECT verified FROM user WHERE id_user = ?";

    if ($stmt = $this->conn->prepare($sql_search)){
      $stmt->bind_param("i", $id);
      $stmt->execute();
      $result = $stmt->get_result();

      while ($row = $result->fetch_assoc()) {
        $verified = $row["verified"];
      }

      if ($verified === 0){
        throw new Exception("User not verified");
      }
      /*free result set */
      $result->free();
      $stmt->close();
      return $verified;
    }
  }

  public function verifyEmail($email, $hash){
    $sql_update = "UPDATE user SET verified = TRUE WHERE email = ?";

    $updatable = false;
    $sql = "SELECT * FROM user WHERE email = ?";
    if ($stmt = $this->conn->prepare($sql)){
      $stmt->bind_param("s", $email);
      $stmt->execute();
      $result = $stmt->get_result();

      $hash_table = "null";
      $data_ini = "null";

      while ($row = $result->fetch_assoc()) {
        if ($row["verified"] > 0){
          throw new Exception("User already verified");
        }
      }
      /*free result set */
      $result->free();
      $stmt->close();
    }

    $updatable = $this->checkExpiration($email, $hash);

    if($updatable){
      if ($stmt = $this->conn->prepare($sql_update)){
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $result = $stmt->get_result();

        if($stmt->affected_rows == 0){
          throw new Exception('Error updating user');
        } else {
          $result = "OK";
        }

        $stmt->close();

        if ($result === "OK"){
          $result = $this->dropVerify($email);
        } else {
          throw new Exception('Error');
        }
      } else {
        throw new Exception('Error');
      }
    }

    return $result;
  }

  public function checkExpiration($email, $hash){
    $sql_check = "SELECT * FROM validator WHERE email = ? AND hash = ?";
    if ($stmt = $this->conn->prepare($sql_check)){
      $stmt->bind_param("ss", $email, $hash);
      $stmt->execute();
      $result = $stmt->get_result();

      $hash_table = "null";
      $data_ini = "null";

      while ($row = $result->fetch_assoc()) {
        $hash_table = $row["hash"];
        $data_ini = $row['data_ct'];
        $data_end = $row['data_end'];
        $dtz = new DateTimeZone("Europe/Lisbon"); //Your timezone
        $now = new DateTime("now", $dtz);
        $date = $now->format("Y-m-d H:i:s");
        if ($date >= $data_end){
          $this->dropVerify($email);
          throw new Exception('You ran out of time');
        }
      }
      /*free result set */
      $result->free();

      if($hash_table === 'null'){
        throw new Exception('You ran out of time');
      } else {
        $updatable = true;
      }

      $stmt->close();
      return $updatable;
    }
  }

  public function addHash($email, $hash){
    $this->dropVerify($email);
    $sql_insert = "INSERT INTO validator(hash, data_end, email) values(?,ADDTIME(now(), '1000'),?)";
    if ($stmt = $this->conn->prepare($sql_insert)){
      $stmt->bind_param("ss", $hash, $email);
      $stmt->execute();

      if($stmt->affected_rows > 0) {
        $result = "OK";
      } else {
        throw new Exception('Error in execution');
      }
      $stmt->close();
      return $result;
    }
    return $result;
  }

  public function dropVerify($email){
    $sql_delete = "DELETE FROM validator WHERE email = ?";
    if ($stmt = $this->conn->prepare($sql_delete)){
      $stmt->bind_param("s", $email);
      $stmt->execute();
      $result = "OK";
    } else {
      throw new Exception('Error');
    }
    return $result;
  }

  private function addUserType($email){
    $sql_user = "SELECT id_user FROM user WHERE email = ?";
    $sql_insert = "INSERT INTO conn_type_user(id_user, id_type) values(?,?)";

    $id_user = "";

    if($stmt = $this->conn->prepare($sql_user)){
      $stmt->bind_param("s", $email);
      $stmt->execute();
      $result = $stmt->get_result();

      while ($row = $result->fetch_assoc()) {
        $id_user = $row["id_user"];
      }
      /*free result set */
      $result->free();

      $stmt->close();
    }

    if($stmt = $this->conn->prepare($sql_insert)){
      $id = 1;
      $stmt->bind_param("ii", $id_user, $id); // user of type user
      $stmt->execute();
      if($stmt->affected_rows == 0){
        throw new Exception('Error adding user');
      }
      $stmt->close();
      return "OK";
    }
  }

  public function login($email, $password){

    $sql_login = "SELECT * FROM user WHERE email = ?";
    $id_user = "";
    if ($stmt = $this->conn->prepare($sql_login)){
      $stmt->bind_param("s", $email);
      $stmt->execute();
      $result = $stmt->get_result();

      while ($row = $result->fetch_assoc()) {
        $id_user = $row["id_user"];
        if (!password_verify($password, $row['pass'])){
          throw new Exception('Wrong password');
        }
      }

      if ($id_user === ""){
        throw new Exception("User does not exist");
      }
      /*free result set */
      $result->free();
      $stmt->close();
      return $id_user;
    }
  }

  public function getTypeUser($id){
    $sql = "SELECT tu.var_type FROM type_user tu INNER JOIN conn_type_user ctu ON ctu.id_type = tu.id_type INNER JOIN user u ON u.id_user = ctu.id_user where u.id_user = ?";
    $type = "";
    if ($stmt = $this->conn->prepare($sql)){
      $stmt->bind_param("i", $id);
      $stmt->execute();
      $result = $stmt->get_result();

      while ($row = $result->fetch_assoc()) {
        $type = $row["var_type"];
      }
      /*free result set */
      $result->free();

      $stmt->close();
      return $type;
    }
  }

  public function deleteUser($id){
    $sql_delete = "DELETE FROM note WHERE id_user = ?;";
    if ($stmt = $this->conn->prepare($sql_delete)){
      $stmt->bind_param("i", $id);
      $stmt->execute();
      $result = "OK";
    } else {
      throw new Exception('Error');
    }

    $sql_delete = "DELETE FROM conn_type_user WHERE id_user = ?;";
    if ($stmt = $this->conn->prepare($sql_delete)){
      $stmt->bind_param("i", $id);
      $stmt->execute();
      $result = "OK";
    } else {
      throw new Exception('Error');
    }

    $sql_delete = "DELETE FROM user WHERE id_user = ?;";
    if ($stmt = $this->conn->prepare($sql_delete)){
      $stmt->bind_param("i", $id);
      $stmt->execute();
      $result = "OK";
    } else {
      throw new Exception('Error');
    }
    return $result;
  }

  public function unverifyUser($id){
    $sql_update = "UPDATE user SET verified = FALSE WHERE id_user = ?";
    if ($stmt = $this->conn->prepare($sql_update)){
      $stmt->bind_param("i", $id);
      $stmt->execute();
      $result = "OK";
    } else {
      throw new Exception('Error');
    }
  }

  public function getAllUsers(){
    $sql_select = "SELECT * FROM user";
    $users;
    if ($stmt = $this->conn->prepare($sql_select)){
      $stmt->execute();
      $result = $stmt->get_result();

      while ($row = $result->fetch_assoc()) {
        $user['id_user'] = $row['id_user'];
        $user['name'] = $row['name'];
        $user['username'] = $row['username'];
        $user['email'] = $row['email'];
        $user['img'] = $row['img'];
        $user['verified'] = $row['verified'];
        $user['type'] = $this->getTypeUser($row['id_user']);
        $user['number_notes'] = $this->getNumberNotes($row['id_user']);
        $users[] = $user;
      }
      /*free result set */
      $result->free();

      $stmt->close();
      return $users;
    }
  }

  public function getNumberNotes($id){
    $sql = "SELECT * FROM note WHERE id_user = ?";

    $count = 0;
    if ($stmt = $this->conn->prepare($sql)){
      $stmt->bind_param("i", $id);
      $stmt->execute();
      $result = $stmt->get_result();
      $count = mysqli_num_rows($result);
      $stmt->close();
      return $count;
    }
  }

  public function getUsername($id){
    $sql_select = "SELECT username FROM user WHERE id_user = ?";
    $username = "";
    if ($stmt = $this->conn->prepare($sql_select)){
      $stmt->bind_param("i", $id);
      $stmt->execute();
      $result = $stmt->get_result();

      while ($row = $result->fetch_assoc()) {
        $username = $row["username"];
      }
      /*free result set */
      $result->free();

      $stmt->close();
      return $username;
    }
  }

  public function contain($email_user){
    $sql_select = "SELECT * FROM user WHERE email = ?";
    $email = "";
    if ($stmt = $this->conn->prepare($sql_select)){
      $stmt->bind_param("s", $email_user);
      $stmt->execute();
      $result = $stmt->get_result();

      while ($row = $result->fetch_assoc()) {
        $email = $row["email"];
      }
      /*free result set */
      $result->free();
      if($email_user !== $email){
        throw new Exception("Email not found");
      }
      $stmt->close();
      return $email;
    }
  }

  public function getUserInfo($id){
    $sql_select = "SELECT * FROM user WHERE id_user = ?";
    $user = array();
    if ($stmt = $this->conn->prepare($sql_select)){
      $stmt->bind_param("i", $id);
      $stmt->execute();
      $result = $stmt->get_result();

      while ($row = $result->fetch_assoc()) {
        $user['name'] = $row['name'];
        $user['username'] = $row["username"];
        $user['email'] = $row['email'];
        $user['img'] = $row['img'];
        $user['number_notes'] = $this->getNumberNotes($row['id_user']);
      }
      /*free result set */
      $result->free();

      $stmt->close();
      return $user;
    }
  }

  public function updateUser($id, $name, $username, $email, $img){
    $sql_update = "UPDATE user SET name = ?, username = ?, email = ?, img = ? WHERE id_user = ?";
    $result = "";
    $user = array();
    if ($stmt = $this->conn->prepare($sql_update)){
      $stmt->bind_param("ssssi", $name, $username, $email, $img, $id);
      $result = "OK";
      $stmt->execute();

      if($stmt->affected_rows == 0){
        throw new Exception('Error updating user');
      }

      $stmt->close();
      return $result;
    }
  }

  public function updatePassword($email, $password){
    $this->dropVerify($email);
    $sql_update = "UPDATE user SET pass = ? WHERE email = ?";
    $pass = password_hash($password, PASSWORD_BCRYPT);

    if ($stmt = $this->conn->prepare($sql_update)){
      $stmt->bind_param("ss", $pass, $email);
      $stmt->execute();
      if($stmt->affected_rows == 0){
        throw new Exception('Error updating user');
      }
      $stmt->close();
      return "OK";
    }
  }

  public function insertNote($name, $text, $id_user){
    $sql_update = "INSERT INTO note (name, text, id_user) values(?,?,?) ";
    $note = $this->getNote($name,$id_user);
    if($note!=null){
      throw new Exception('Note with same name already exists');
    }else if ($stmt = $this->conn->prepare($sql_update)){
      $stmt->bind_param("ssi", $name, $text , $id_user);
      $stmt->execute();
      if($stmt->affected_rows == 0){
        throw new Exception('Error inserting note');
      }else{
        $result = "OK";
      }
      $stmt->close();
      return $result;
    }
  }

  public function deleteNote($id_note){
    $sql_update = "DELETE FROM note WHERE id = ?";
    if ($stmt = $this->conn->prepare($sql_update)){
      $stmt->bind_param("i", $id_note);
      $stmt->execute();
      if($stmt->affected_rows == 0){
        throw new Exception('Error deleting Note');
      }else{
        $result = "OK";
      }
      $stmt->close();
      return $result;
    }
  }

  public function getNote($name, $id_user){
    $sql_update = "SELECT * FROM note WHERE name = ? AND id_user = ? ";

    if($stmt = $this->conn->prepare($sql_update)){
      $stmt->bind_param("si", $name, $id_user);
      $stmt->execute();
      $result = $stmt->get_result();
    }

    $note = null;
    while ($row = $result->fetch_assoc()) {
      $note['id'] = $row['id'];
      $note['name'] = $row['name'];
      $note['text'] = $row["text"];
      $note['last_edit'] = $row['last_edit'];
      $note['published_on'] = $row['published_on'];
    }
    $result->free();
    $stmt->close();
    return $note;
  }

  public function getNotes($id_user){
    $sql_update = "SELECT * FROM note WHERE id_user = ? ORDER BY last_edit DESC";

    if($stmt = $this->conn->prepare($sql_update)){
      $stmt->bind_param("i", $id_user);
      $stmt->execute();
      $result = $stmt->get_result();
    }

    $notes = null;
    while ($row = $result->fetch_assoc()) {
      $note['id'] = $row['id'];
      $note['name'] = $row['name'];
      $note['text'] = $row["text"];
      $note['last_edit'] = $row['last_edit'];
      $notes[] = $note;
    }
    $result->free();
    $stmt->close();
    return $notes;
  }

  public function updateNote($old_name, $new_name, $text, $id_user){
    $sql_update = "UPDATE note SET name = ?, text = ?, last_edit = CURRENT_TIMESTAMP WHERE name = ? AND id_user = ? ";
    if ($stmt = $this->conn->prepare($sql_update)){
      $stmt->bind_param("sssi", $new_name, $text , $old_name, $id_user);
      $stmt->execute();
      if($stmt->affected_rows == 0){
        throw new Exception('Error updating note');
      }else{
        $result = "OK";
      }
      $stmt->close();
      return $result;
    }
  }

}
?>