<?php

class Validator{

  public function sanitize($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }

  public function validate_password($data)
  {
    $password = $this->sanitize($data);
    if (strlen($password) < 8) {
      return "Password to short";
    } elseif (strlen($password) > 20) {
      return "Password to big";
    }/* elseif (!preg_match("#[0-9]+#", $password)) {
      return "Your Password Must Contain At Least 1 Number!";
    } elseif (!preg_match("#[A-Z]+#", $password)) {
      return "Your Password Must Contain At Least 1 Capital Letter!";
    } elseif (!preg_match("#[a-z]+#", $password)) {
      return "Your Password Must Contain At Least 1 Lowercase Letter!";
    }*/
  }

  public function validate_email($data)
  {
    $email = $this->sanitize($data);
    if (empty($email)){
      return "Email can't be empty";
    }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      return 'Invalid email format';
    }
  }

  public function validate_name($data)
  {
    $name = $this->sanitize($data);
    if (empty($name)){
      return "Name can't be empty";
    }
    if (strlen($name) < 2) {
      return "Name too short";
    }
    if (strlen($name) > 11) {
      return "Name too long";
    }
    if (preg_match("/[0-9]+/", $name) > 0) {
      return "Name can't contain numbers";
    }
  }

  public function validate_note_name($data)
  {
    $name = $this->sanitize($data);
    if (empty($name)){
      return "Name can't be empty";
    }
    if (strlen($name) < 2) {
      return "Name too short";
    }
    if (strlen($name) > 20) {
      return "Name too long";
    }
  }

  public function validate_text($data)
  {
    $name = $this->sanitize($data);
    if (empty($name)){
      return "Text can't be empty";
    }
    if (strlen($name) > 3000) {
      return "Text too big( limit 3000 characters)";
    }
  }

  public function validate_username($data)
  {
    $name = $this->sanitize($data);
    if (empty($name)){
      return "Username can't be empty";
    }
    if (strlen($name) < 4) {
      return "Username too short";
    }
  }
}

?>