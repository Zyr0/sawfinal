<?php

  $mysql = new MysqlConnection();
  $hasMysqlErr = false;
  $err = false;
  $sended = false;
  $email = "";

  session_destroy();
  if (isset($_GET['email']) && isset($_GET['h'])){

    try {
      $email = $_GET['email'];
      $mysql->verifyEmail($email, $_GET['h']);
    } catch(Exception $e){
      $hasMysqlErr = true;
      $mysqlErrMessage = $e->getMessage();
      error_log("Error  " . $mysqlErrMessage, 0);
    }
  } else {
    header("location: ?t=home");
  }

  if ($_SERVER['REQUEST_METHOD'] == "POST"){
    if (isset($_POST['send_email'])){
      $email = $_POST['send_email'];
      $result = $mysql->dropVerify($email);
      if ($result == "OK"){

        $val = $email . rand(0, rand(0, 99999));
        $hash = sha1($val);

        $mysql->addHash($email, $hash);

        $to = '' . $email . '';
        $subject = 'I Saw Notes';
        $message='Please click this link to activate your account: https://www.saw.com/?t=verify&email=' . $email . '&h=' . $hash;

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
        $headers .= 'From: I Saw Notes <dwdmsawfinalcj@gmail.com>' . "\r\n";
        mail($to, $subject, $message, $headers);
        $sended = true;
      }
    }
  }
?>

<section class="col-12">
  <?php if($hasMysqlErr): ?>
  <div class="alert alert-danger alert-dismissable fade show mt-2">
    <?php if ($mysqlErrMessage === "Unable to verify maby you ran out of time"): ?>
    <form class="form-inline d-block" method="post">
      <strong>Who!</strong> <?php echo $mysqlErrMessage; ?> &#32;
      <input type="hidden" name="send_email" value="<?php echo $email ?>">
      <input type="submit" class="btn btn-outline-primary" value="Send new email">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </form>
    <?php elseif($sended): ?>
    <div class="alert alert-success alert-dismissable fade show mt-2">
      <strong>Who!</strong> The email has been sended
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php else: ?>
    <strong>Who!</strong> <?php echo $mysqlErrMessage; ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    <?php endif ?>
  </div>
  <?php else: ?>
  <div class="alert alert-success alert-dismissable fade show mt-2">
    <strong>Who!</strong> You can login <a href="?t=login">here</a>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <?php endif; ?>
</section>