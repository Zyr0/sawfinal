<?php

  $mysqlErrMessage = "";
  $hasMysqlErr = false;
  $sended = false;

  if($_SERVER["REQUEST_METHOD"] == "POST") {
    $mysql = new MysqlConnection();
    $validator = new Validator();

    if (isset($_POST['send_email'])){
      $email = $_POST['send_email'];
      try {
        $result = $mysql->dropVerify($email);
        if ($result == "OK"){

          $val = $email . rand(0, rand(0, 99999));
          $hash = sha1($val);


          $mysql->addHash($email, $hash);
          $to = '' . $email . '';
          $subject = 'I Saw Notes';
          $message='Please click this link to activate your account: https://www.saw.com/?t=verify&email=' . $email . '&h=' . $hash;

          $headers  = 'MIME-Version: 1.0' . "\r\n";
          $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
          $headers .= 'From: I Saw Notes <dwdmsawfinalcj@gmail.com>' . "\r\n";
          mail($to, $subject, $message, $headers);
          $sended = true;
        }
      } catch (Exception $e) {
          $hasMysqlErr = true;
          $mysqlErrMessage = $e->getMessage();
        }
    } else {
      $email = $validator->sanitize($_POST['email']);
      $pass = $validator->sanitize($_POST['pass']);

      try {
        $id_user = $mysql->login($email, $pass);
        $verified = $mysql->isVerified($id_user);
        if ($id_user > 0 && $verified != 0){
          if(!empty($_POST["remember"])) {
            $hour = time() + 3600;
            setcookie('email', $email, $hour);
          } else {
            $past = time() - 100;
	        	setcookie("email", "", $past);
          }
          $_SESSION['id_user'] = $id_user;
          header("location: ?t=profile");
        }
      } catch(Exception $e){
        $hasMysqlErr = true;
        $mysqlErrMessage = $e->getMessage();
        error_log("Error  " . $mysqlErrMessage, 0);
      }
    }
  }
?>
<section class="col-12">
  <?php if($hasMysqlErr): ?>
    <div class="alert alert-danger alert-dismissable fade show mt-2">
      <?php if ($mysqlErrMessage === "User not verified"): ?>
      <form class="form-inline d-block" method="post">
        <strong>Who!</strong> <?php echo $mysqlErrMessage; ?> &#32;
        <input type="hidden" name="send_email" value="<?php echo $email ?>">
        <input type="submit" class="btn btn-outline-primary" value="Send new email">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </form>
      <?php else: ?>
    <strong>Who!</strong> <?php echo $mysqlErrMessage; ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    <?php endif ?>
  </div>
  <?php elseif($sended): ?>
  <div class="alert alert-success alert-dismissable fade show mt-2">
    <strong>Who!</strong> The email has been sended
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <?php endif; ?>
</section>

<section class="col-6 col-xs-12" id="login">
  <h1 class="display-3 text-center">Log in</h1>
  <form class="mt-4 needs-validation" role="form" method="post" id="login-form" autocomplete="off" accept-charset='UTF-8' novalidate>
    <div class="form-group">
      <label for="email" class="sr-only">Email</label>
      <input type="email" name="email" id="email" class="form-control" placeholder="somebody@example.com"
      value="<?php if(isset($_COOKIE["email"])) { echo $_COOKIE["email"]; } ?>" required>
    </div>
    <div class="form-group">
      <label for="pass" class="sr-only">Password</label>
      <input type="password" name="pass" id="pass" class="form-control" placeholder="Password" require>
    </div>
    <div class="field-group">
      <input type="checkbox" name="remember" id="remember"
        <?php if(isset($_COOKIE["email"])): ?>
          checked
        <?php endif; ?>
      />
		  <label for="remember-me">Remember me</label>
    </div>
    <div class="row">
    <div class="col">
      <a href="#myModal" data-toggle="modal">Forget my Password</a>
      <div class="text-right" style="margin-top: -20px">
        <input type="submit" id="btn-login" class="btn btn-primary btn-lg" value="Log in">
      </div>
    </div>
    </div>
  </form>
</section>

<div class="text-left">

<!-- Modal HTML -->
<div id="myModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Forget my Password</h4>
      </div>
      <div class="modal-body">
      <form method="GET" action="?t=recovery">
        <div class="form-group">
          <input type="hidden" name="t" value="recovery">
          <label for="email" class="sr-only">Email</label>
          <input type="email" name="email" id="email" class="form-control" placeholder="somebody@example.com" required>
          <input type="hidden" name="h" value=<?php echo sha1(rand(0, rand(0, 99999))); ?>>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">X</button>
          <input type="submit" class="btn btn-primary btn-lg" value="Send Email">
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
</div>

