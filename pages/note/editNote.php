<?php

  $hasMysqlErr = false;
  $result = "no";
  $err = array("Invalid" => "<strong>Invalid Form</strong>");
  $hasErr = false;

  if(isset($_GET['name'])){
    $note_name=($_GET['name']);
    $note = $mysql->getNote($note_name,$_SESSION['id_user']);
    if($note!=null){
      $old_name=$note['name'];
      $old_text=$note['text'];
      $last_edit=$note['last_edit'];
      $published_on=$note['published_on'];
    }
  }

  if($_SERVER["REQUEST_METHOD"] == "POST") {
    $mysql = new MysqlConnection();
    $validator = new Validator();

    try{
      $err['note_name'] = $validator->validate_note_name($_POST['note_name']);
      $err['note_text'] = $validator->validate_text($_POST['note_text']);
      $note_name = $validator->sanitize($_POST['note_name']);
      $note_text = $validator->sanitize($_POST['note_text']);


      if ($err['note_name'] != "" or $err['note_text'] != ""){
        $hasErr = true;
      }

      if (!$hasErr){
          $result = $mysql->updateNote($old_name, $note_name, $note_text, $_SESSION['id_user']);
      }
    }
    catch(Exception $e){
      $hasMysqlErr = true;
      $mysqlErrMessage = $e->getMessage();
      error_log("Error  " . $mysqlErrMessage, 0);
    }
  }
?>
<!-- errors -->
  <div class="col-12">
    <?php if($hasMysqlErr or $hasErr): ?>
    <div class="alert alert-danger alert-dismissable fade show mt-2">
      <strong>Who!</strong>
      <?php
      if ($hasMysqlErr) {
        echo $mysqlErrMessage;
      } else {
        foreach($err as $e => $e_val){
          if ($e_val != ""){
            echo $e_val . " ";
          }
        }
      }
      ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php endif; ?>
    <?php if($result === "OK" && !$hasMysqlErr): ?>
    <div class="alert alert-success alert-dismissable fade show mt-2">
      <strong>Note edited sucessfully!</strong>
      <?php
        $note_name=($_GET['name']);
        $note = $mysql->getNote($note_name,$_SESSION['id_user']);
        if($note!=null){
          $old_name=$note['name'];
          $old_text=$note['text'];
        }
      ?>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php endif; ?>
  </div>
<!-- end errors -->

<section class="col-8 mt-4">
  <h4><i class="fa fa-edit"></i><i> Edit Note</i> </h4>
  <form class="mt-4 needs-validation" role="form" method="post" id="note-form" autocomplete="off" accept-charset='UTF-8' novalidate>
    <div class="form-group">
      <input type="text" class="form-control" name="note_name" placeholder="Name" required value="<?php echo $old_name; ?>">
    </div>
    <div class="form-group">
      <label for="note_text">Note </label>
        <textarea class="form-control" name="note_text" rows="10"  required>
          <?php echo htmlspecialchars($old_text); ?>
        </textarea>
    </div>
    <button type="submit" href="" class="btn btn-primary" name="submit" value="Submit" id="submit_form">Save      note</button>
  </form>
</section>
