
<?php

$mysql = new MysqlConnection();

$notes[] = $mysql->getNotes($_SESSION['id_user']);
$count=count($notes[0]);

if(isset($_GET['name'])){
  $note_name = ($_GET['name']);
  $note = $mysql->getNote($note_name, $_SESSION['id_user']);
  if($note!=null){
    $note_id = $note['id'];
    $note_name = $note['name'];
    $note_text = $note['text'];
    $last_edit = $note['last_edit'];
    $published_on = $note['published_on'];
  }
}
?>

<section class="col-12">
  <div id="mySidenav" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <?php $i=0; while($i < $count): ?>
      <a name="note" href="?t=selected&name=<?php echo $notes[0][$i]['name'] ?>"> <?php echo $notes[0][$i]['name'] ?> </a>
    <?php $i++; endwhile; ?>
  </div>
  <div class="row">
    <div class="col-md-3 col-xs-6">
      <button class="btn btn-danger btn-notes" style="cursor:pointer;" onclick="openNav()">&#9776; My Notes</button>
    </div> <!-- /.col-6 -->
    <div class="col-md-9 col-xs-12 mt-4">
      <div id="card_id" class="card text-left">
        <div class="card-header">
        <h5> <i class="fa fa-file-text"></i> <?php echo $note_name ?></h5>
        </div>
        <div class="card-body">
          <h5 class="card-title"> </h5>
          <p class="card-text"><?php echo $note_text ?> </p>

        </div>
      <div class="card-footer text-muted">
        <div class="row">
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-3">
                <a href="?t=editnote&name=<?php echo $note_name ?>" type="submit "id="btn_edit" class="fa fa-edit btn btn btn-primary">
                  Edit
                </a>
              </div>
              <div class="col-md-4">
                <a href="?t=del&nm=<?php echo $note_id ?>" type="submit "id="btn_remove" class="fa fa-times btn btn-danger">
                  Remove
                </a>
              </div>
            </div>
          </div>
          <div class="col-md-6">
          Last Edit  <?php echo $last_edit ?>
          </div></div>
      </div>
    </div>
    </div>
  </div>
</section>

<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
  document.getElementById("card_id").style.backgroundColor = "rgba(0,0,0,0.0)";
  document.getElementById("btn_edit").style.backgroundColor = "rgba(0,0,0,0.0)";
  document.getElementById("btn_remove").style.backgroundColor = "rgba(0,0,0,0.0)";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.body.style.backgroundColor = "white";
  document.getElementById("card_id").style.backgroundColor = "white";
  document.getElementById("btn_edit").style.backgroundColor = "#428bca";
  document.getElementById("btn_remove").style.backgroundColor = "#d9534f";
}
</script>
