<?php
  include 'pages/class/MysqlConnection.php';
  include 'pages/class/Validator.php';

  // $mysql = new MysqlConnection();
  // $mysql->createTables();

  session_start();
  $title = "";
  $content = "";
  $needsLog = true;
  $error = false;

  if (isset($_SESSION['id_user'])){
    $needsLog = false;
  }

  if (isset($_GET['t'])) {
    switch ($_GET['t']) {
      case 'recovery':
        $needsLog = false;
        $title = 'Password Reset';
        $content = "pages/registo/recovery.php";
        break;
      case 'register':
        $needsLog = false;
        $title = 'Registration';
        $content = "pages/registo/registo.php";
        break;
      case 'login':
        $needsLog = false;
        $title = 'Login';
        $content = "pages/login/login.php";
        break;
      case 'verify':
        $needsLog = false;
        $title = 'Verify Email';
        $content = 'pages/login/verify.php';
        break;
      case 'recPassword':
        $needsLog = false;
        $title = "New Password";
        $content = "pages/registo/recPassword.php";
        break;
      case 'logout':
        $title = "Logout";
        $content = "pages/login/logout.php";
        break;
      case 'profile':
        $title = "Profile";
        $content = "pages/user/profile.php";
        break;
      case 'edit_profile':
        $title = "Edit Profile";
        $content = "pages/user/edit.php";
        break;
      case 'note':
        $title = "Add new note";
        $content = "pages/note/newNote.php";
        break;
      case 'editnote':
        $title = "Edit Note";
        $content = "pages/note/editNote.php";
        break;
        case 'del':
        $title = "Notes List";
        $content = "pages/note/listNotes.php";
        break;
      case 'listNote':
        $title = "Notes List";
        $content = "pages/note/listNotes.php";
        break;
      case 'selected':
        $title = "Notes List";
        $content = "pages/note/selectedNote.php";
        break;
      case 'listUsers':
        $title = 'List Users';
        $content = "pages/admin/listUsers.php";
        break;
      default:
        $needsLog = false;
        $title = 'Home';
        $content = "pages/home/home.php";
        break;
    }
    if ($needsLog){
      $error = true;
      $title = "Login";
      $content = "pages/login/login.php";
    }
  } else {
    header("Location: ?t=home");
  }

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
  <base href="https://www.saw.com/">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title><?php echo $title ?></title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="resources/styles.css">
  <link rel="stylesheet" href="resources/show.css">
</head>
<body>
  <?php include "nav.php" ?>

  <main class="container">
    <?php if ($error): ?>
    <div class="alert alert-danger alert-dismissable fade show" style="margin-top: 2em">
      <strong>Who!</strong> You need login
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <?php endif; ?>
    <section class="row align-items-center justify-content-center">
      <?php include $content ?>
    </section>
  </main>

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>